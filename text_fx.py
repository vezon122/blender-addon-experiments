# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

# <pep8-80 compliant>
# Copyright 2014 Bassam Kurdali 
import bpy
from bpy.app.handlers import persistent
from bpy.types import TextCurve
from random import random

bl_info = {
    'name': 'Text FX',
    'author': 'Bassam Kurdali',
    'version': '1.0',
    'blender': (2, 7, 2),
    'location': 'Properties Editor, Text Context',
    'description': 'Text Animation Effects',
    'url': 'http://urchn.org',
    'category': 'Text'}

__bpydoc__ = """
Text Effects Animation Tool
Allows animating multiple segments of Text:
Add as many text segments you like, based either on text datablocks, or by
directly typing into the UI, and then, for each consequetive segment, add
one, none or more of the following Effects:
Typing: Segment 'types' over time, uses a function curve.
Blink: Segment can be turned on or off using a function curve.
Font Option: Bold, Italic, Underline or small caps can be animated.
Replace Text: Text can be changed over time.
"""

FCURVE_PATH = "text_segments[{}].text_effects[{}].anim"
FORMATTING = [
    'material_index', 'use_bold', 'use_italic',
    'use_small_caps', 'use_underline']

# ###################  Property Helper and Update Functions ###################


def go_forward(sequence, text_effect):
    if text_effect.use_count:
        return[[0, text_effect.count]]
    else:
        return [[0, int(text_effect.anim * len(sequence))]]


def go_backward(sequence, text_effect):
    length = len(sequence)
    if text_effect.use_count:
        return[[length - text_effect.count, length]]
    else:
        return [[int((1 - text_effect.anim) * length), length]]


def go_blink(sequence, text_effect):
    if text_effect.use_count:
        return [[0, len(sequence) * int(text_effect.count > 0)]]
    else:
        return [[0, len(sequence) * int(text_effect.anim > 0.99)]]


def go_random(sequence, text_effect):
    return [[0, len(sequence)]]

directions = {
    'FORWARD': (
        'Forward', 'Animate Effect From Beginning of Text', go_forward, 0),
    'BACKWARD': (
        'Backward', 'Animate Effect from End of Text', go_backward, 1),
    'BLINK': (
        'Blink', 'Turn Text On or Off', go_blink, 2),
    'RANDOM': (
        'Random', 'Animate Random Characters', go_random, 3)
    }


def anim_visible(text_effect, current_text, current_format):
    slicer = directions[text_effect.progression][2](
        current_text, text_effect)
    return_text = ""
    return_format = {attr:[] for attr in current_format}
    for slices in slicer:
        return_text = "{}{}".format(
            return_text, current_text[slices[0]:slices[1]])
        for attr in current_format:
            return_format[attr].extend(
                current_format[attr][slices[0]: slices[1]])
    current_text = return_text
    current_format = return_format
    return (current_text, current_format)


def anim_blank(text_effect, current_text, current_format):
    slicer = directions[text_effect.progression][2](
        current_text, text_effect)
    text_result = ''.ljust(len(current_text) - slicer[-1][1])
    start = 0
    for s in slicer:
        text_result = "{}{}{}".format(
            ''.ljust(s[0]-start), current_text[s[0]:s[1]], text_result)
        start = s[1]
    current_text = text_result
    return (current_text, current_format)


def anim_material(text_effect, current_text, current_format):
    slicer = directions[text_effect.progression][2](
        current_text, text_effect)
    for slices in slicer:
        for index in range(slices[0], slices[1]):
            current_format['material_index'][index] =\
                text_effect.material_index        
    return (current_text, current_format)


def anim_attribute(slicer, current_format, attribute, keep):
    for slices in slicer:
        for index in range(slices[0], slices[1]):
            current_format[attribute][index] =\
                keep or not current_format[attribute][index]
    return current_format


def anim_bold(text_effect, current_text, current_format):
    return (current_text, anim_attribute(
        directions[text_effect.progression][2](current_text, text_effect),
        current_format, 'use_bold', not text_effect.invert))


def anim_italic(text_effect, current_text, current_format):
    return (current_text, anim_attribute(
        directions[text_effect.progression][2](current_text, text_effect),
        current_format, 'use_italic', not text_effect.invert))


def anim_underline(text_effect, current_text, current_format):
    return (current_text, anim_attribute(
        directions[text_effect.progression][2](current_text, text_effect),
        current_format, 'use_underline', not text_effect.invert))


def anim_smallcaps(text_effect, current_text, current_format):
    return (current_text, anim_attribute(
        directions[text_effect.progression][2](current_text, text_effect),
        current_format, 'use_small_caps', not text_effect.invert))

def anim_math_add(text_effect, current_text, current_format):
    return(
        str(float(current_text) + text_effect.anim)[:len(current_text)] ,
        current_format)

targets = {
    'VISIBLE': (
        'Visible', 'Animate Character Visiblity', anim_visible, 0),
    'BLANK': (
        'Blank', 'Replace Character with Blank', anim_blank, 1),
    'MATERIAL': (
        'Material', 'Animate Character Material Slot', anim_material, 2),
    'BOLD': (
        'Bold', 'Animate Bold Option', anim_bold, 3),
    'ITALIC': (
        'Italic', 'Animate Italic Option', anim_italic, 4),
    'UNDERLINE': (
        'Underline', 'Animate Underline Option', anim_underline, 5),
    'SMALLCAPS': (
        'Small Caps', 'Animate Small Caps Option', anim_smallcaps, 6),
    'MATH_ADD': (
        'Math Add', 'Add Fcurve to Data', anim_math_add, 7)
    }


def text_segment_format_to_list(text_segment_format):
    '''Return formats as list of tuples'''
    return {
        i: [getattr(char_fmt, i) for char_fmt in text_segment_format]
        for i in FORMATTING}


def list_format_to_body(body_format, list_format):
    '''Copy the current effects format to the text body format'''
    for attr in list_format:
        for char_fmt, input_format in zip(body_format, list_format[attr]):
            setattr(char_fmt, attr, input_format)


def markup_parse(raw_string):
    '''Placeholder for markup parser'''
    return (raw_string, {attr:[0] * len(raw_string) for attr in FORMATTING})


def uptext(text):
    '''Build the Text up from it's current state.'''
    output_string = ""
    output_format = {attr:[] for attr in FORMATTING}
    enabled_segments = (seg for seg in text.text_segments if seg.use_enabled)
    for text_segment in enabled_segments:
        if text_segment.use_datablock:
            current_text, current_format = markup_parse(
                bpy.data.texts[text_segment.source_data].as_string())
        else:
            current_text = text_segment.source_text
            current_format = text_segment_format_to_list(
                text_segment.source_format)
        enabled_effects = (
            efct for efct in text_segment.text_effects if efct.use_enabled)
        for text_effect in enabled_effects:
            current_text, current_format = targets[text_effect.target][2](
                text_effect,
                current_text,
                current_format)
        output_string = "{}{}".format(output_string, current_text)
        for attr in FORMATTING:
            output_format[attr].extend(current_format[attr])
    text.body = output_string
    list_format_to_body(text.body_format, output_format)


@persistent   
def text_fx_update_frame(scene):
    '''Sadly we need this for frame change updating.'''
    for text in scene.objects:
        if text.type == 'FONT' and text.data.use_text_FX:
            uptext(text.data)


def update_func(self, context):
    '''Updates when changing the value.'''
    uptext(self.id_data)


def update_seed(self, context):
    '''Updates when changing the seed.'''
    self.random = random()


def get_text_blocks(self, context):
    '''Grab Text Datablocks and return items tuple'''
    return [tuple([block.name] * 3) for block in bpy.data.texts]


def make_backup(self, context):
    '''Back up the current string once we use Text Effects'''
    pass


def add_text_segment(context, name="Segment"):
    '''Add a new text Segment'''
    # Add new Segment
    text = context.active_object.data
    new_segment = text.text_segments.add()
    new_segment.name = name
    # Point Active to New Text Segment
    text.active_text_segment = len(text.text_segments) - 1
    return new_segment


def add_text_effect(context, name="Effect"):
    '''Add a new text Effect'''
    # Add new Effect
    text = context.active_object.data
    segment = text.text_segments[text.active_text_segment]
    new_effect = segment.text_effects.add()
    new_effect.name = name
    # Point Active to New Text Effect:
    segment.active_text_effect = len(segment.text_effects) - 1
    return new_effect


def copy_text_to_segment(context):
    '''build text segment from a text curve'''
    text = context.active_object.data
    segment = text.text_segments[text.active_text_segment]
    segment.source_text = text.body
    segment.source_format.clear()
    for character_format in text.body_format:
        new_format = segment.source_format.add()
        for attr in dir(character_format):
            if attr in dir(new_format) and \
                not attr.startswith('__') and not 'rna' in attr:

                setattr(new_format, attr, getattr(character_format, attr))

# #########################  Property Definitions ############################


class TextFormat(bpy.types.PropertyGroup):
    material_index = bpy.props.IntProperty(options=set())
    use_bold = bpy.props.BoolProperty(options=set())
    use_italic = bpy.props.BoolProperty(options=set())
    use_small_caps = bpy.props.BoolProperty(options=set())
    use_underline = bpy.props.BoolProperty(options=set())


class TextEffect(bpy.types.PropertyGroup):
    anim = bpy.props.FloatProperty(
        min=0.0, max=1.0, default=1.0,
        update=update_func, options={'ANIMATABLE'})
    count = bpy.props.IntProperty(
        min=0, update=update_func, options={'ANIMATABLE'})
    target = bpy.props.EnumProperty(
        items=sorted(
            [(i, targets[i][0], targets[i][1], targets[i][3]) 
            for i in targets],
            key=lambda x:x[3]),
        options=set())
    progression = bpy.props.EnumProperty(
        items=sorted(
            [(i, directions[i][0], directions[i][1], directions[i][3])
            for i in directions],
            key=lambda x:x[3]),
        options=set())
    invert = bpy.props.BoolProperty(default=False, options=set())
    seed = bpy.props.IntProperty(default=0, options=set())
    use_enabled = bpy.props.BoolProperty(default=True, options=set())
    material_index = bpy.props.IntProperty(default=0)
    use_count = bpy.props.BoolProperty(default=False)


class TextSegment(bpy.types.PropertyGroup):
    source_text = bpy.props.StringProperty(options=set())
    source_data = bpy.props.EnumProperty(items=get_text_blocks, options=set())
    source_format = bpy.props.CollectionProperty(type=TextFormat)
    use_datablock = bpy.props.BoolProperty(default=False, options=set())
    text_effects = bpy.props.CollectionProperty(type=TextEffect)
    active_text_effect = bpy.props.IntProperty(default=0, options=set())
    use_enabled = bpy.props.BoolProperty(default=True, options=set())


def add_properties():
    '''Add Properties to TextCurve'''
    bpy.utils.register_class(TextFormat)
    bpy.utils.register_class(TextEffect)
    bpy.utils.register_class(TextSegment)
    bpy.types.TextCurve.backup_text = bpy.props.StringProperty(
        default="", update=make_backup, options=set())
    bpy.types.TextCurve.use_text_FX = bpy.props.BoolProperty(options=set())
    bpy.types.TextCurve.active_text_segment = bpy.props.IntProperty(
        default=0, options=set())
    bpy.types.TextCurve.text_segments = bpy.props.CollectionProperty(
        type=TextSegment)


def del_properties():
    '''Remove Properties from TextCurve'''
    del(bpy.types.TextCurve.text_segments)
    del(bpy.types.TextCurve.active_text_segment)
    del(bpy.types.TextCurve.use_text_FX)
    del(bpy.types.TextCurve.backup_text)
    bpy.utils.unregister_class(TextFormat)
    bpy.utils.unregister_class(TextEffect)
    bpy.utils.unregister_class(TextSegment)

# ###############################  Operators  ################################


class AddTextSegment(bpy.types.Operator):
    '''Add a new text segment'''
    bl_idname = 'curve.add_text_segment'
    bl_label = 'Add Text Segment'

    name = bpy.props.StringProperty(default="Segment", name="Name")

    def invoke(self, context, event):
        context.window_manager.invoke_props_dialog(self)
        return {'RUNNING_MODAL'}

    def execute(self, context):
        add_text_segment(context, self.properties.name)
        return {'FINISHED'}

    def draw(self, context):
        props = self.properties
        layout = self.layout
        row = layout.row()
        row.prop(props, "name")


class AddTextEffect(bpy.types.Operator):
    '''Add a new Effect'''
    bl_idname = 'curve.add_text_effect'
    bl_label = 'Add Text Effect'

    name = bpy.props.StringProperty(default="Effect", name="Name")

    def invoke(self, context, event):
        context.window_manager.invoke_props_dialog(self)
        return {'RUNNING_MODAL'}

    def execute(self, context):
        add_text_effect(context, self.properties.name)
        return {'FINISHED'}

    def draw(self, context):
        props = self.properties
        layout = self.layout
        row = layout.row()
        row.prop(props, "name")


class CopyTextToSegment(bpy.types.Operator):
    '''Copy Current Text Body and Format to Active Text Segment'''
    bl_idname = 'curve.copy_text_to_segment'
    bl_label = 'Copy Text to Text FX Segment'

    def execute(self, context):
        copy_text_to_segment(context)
        return {'FINISHED'}


def remove_and_reshuffle(
    fcurves, base_path, old_index, top_index, remove=True):
    '''Remove FCurves for deleted item and reshuffle subsequent ones down.'''
    path = base_path.format(old_index)
    for fcurve in fcurves:
            if fcurve.data_path.startswith(path) and remove:
                fcurves.remove(fcurve)

    for i in range(old_index + 1, top_index):
        path = base_path.format(i)
        newpath = base_path.format(i - 1)
        for fcurve in fcurves:
            if fcurve.data_path.startswith(path):
                fcurve.data_path = fcurve.data_path.replace(path, newpath)


class DelTextSegment(bpy.types.Operator):
    '''Delete the active text segment'''
    bl_idname = 'curve.delete_text_segment'
    bl_label = 'Delete Text Segment'

    @classmethod
    def poll(cls, context):
        return context.active_object.data.text_segments

    def execute(self, context):
        # Delete active segment
        text = context.active_object.data
        text.text_segments.remove(text.active_text_segment)
        # Fixuup the active segment
        old_index = text.active_text_segment
        if old_index:
            text.active_text_segment -= 1
        # Remove related fcurves and fix paths

        if text.animation_data:
            anim = text.animation_data.action
            path = FCURVE_PATH[:20]
            if anim:
                remove_and_reshuffle(
                    anim.fcurves, path,
                    old_index, len(text.text_segments) + 1)
            remove_and_reshuffle(
                text.animation_data.drivers, path,
                old_index, len(text.text_segments) + 1, remove=False) 
        return {'FINISHED'}


class DelTextEffect(bpy.types.Operator):
    '''Delete the active text effect'''
    bl_idname = 'curve.delete_text_effect'
    bl_label = 'Delete Text Segment'

    @classmethod
    def poll(cls, context):
        text = context.active_object.data
        return text.text_segments[text.active_text_segment].text_effects

    def execute(self, context):
        text = context.active_object.data
        active_text_segment = text.active_text_segment
        text_segment = text.text_segments[active_text_segment]
        text_segment.text_effects.remove(text_segment.active_text_effect)
        # Fixuup the active effect
        old_index = text_segment.active_text_effect
        if old_index:
            text_segment.active_text_effect -= 1
        # Remove fcurve adn fix paths
        if text.animation_data:
            anim = text.animation_data.action
            path = FCURVE_PATH.format(active_text_segment, "{}")
            if anim:
                remove_and_reshuffle(
                    anim.fcurves, path,
                    old_index, len(text_segment.text_effects) + 1)
            remove_and_reshuffle(
                text.animation_data.drivers, path,
                old_index, len(text_segment.text_effects) + 1, remove=False) 
        return {'FINISHED'}


def swap_curves(fcurves, match, a, b):
    '''Swap Data Path references from original a, b curves.'''
    a_prefix = match.format(a)
    b_prefix = match.format(b)
    a_curves = [
        fcurve for fcurve in fcurves if fcurve.data_path.startswith(a_prefix)]
    b_curves = [
        fcurve for fcurve in fcurves if fcurve.data_path.startswith(b_prefix)]
    for fcurve in a_curves:
        fcurve.data_path = fcurve.data_path.replace(a_prefix, b_prefix)
    for fcurve in b_curves:
        fcurve.data_path = fcurve.data_path.replace(b_prefix, a_prefix)


def swap_animation_data(animation_data, data_path, active, increment):
    '''Swap/fix data paths for drivers and action.'''
    if animation_data:
        action = animation_data.action
        next = active + increment
        swap_curves(animation_data.drivers, data_path, active, next)
        if action:
            swap_curves(action.fcurves, data_path, active, next)


class MoveDownTextSegment(bpy.types.Operator):
    '''Move the active text segment Down'''
    bl_idname = 'curve.move_down_text_segment'
    bl_label = 'Move Text Segment Down'

    @classmethod
    def poll(cls, context):
        text = context.active_object.data
        text_segments = text.text_segments
        return text_segments and \
            text.active_text_segment < len(text_segments) - 1      

    def execute(self, context):
        # Move down the segment
        text = context.active_object.data
        active_text_segment = text.active_text_segment
        text.text_segments.move(active_text_segment, active_text_segment + 1)
        # Point to the new index
        text.active_text_segment += 1
        # Fixup FCurve paths
        swap_animation_data(
            text.animation_data, FCURVE_PATH[:20], active_text_segment, 1)
        return {'FINISHED'}


class MoveUpTextSegment(bpy.types.Operator):
    '''Move the active text segment Up'''
    bl_idname = 'curve.move_up_text_segment'
    bl_label = 'Move Text Segment Up'

    @classmethod
    def poll(cls, context):
        text = context.active_object.data
        text_segments = text.text_segments
        return text_segments and text.active_text_segment > 0

    def execute(self, context):
        # Move up the segment
        text = context.active_object.data
        active_text_segment = text.active_text_segment
        text.text_segments.move(active_text_segment, active_text_segment - 1)
        # Point to the new index
        text.active_text_segment -= 1
        # Fixup FCurve paths
        swap_animation_data(
            text.animation_data, FCURVE_PATH[:20], active_text_segment, -1)
        return {'FINISHED'}


class MoveDownTextEffect(bpy.types.Operator):
    '''Move the active text effect Down'''
    bl_idname = 'curve.move_down_text_effect'
    bl_label = 'Move Text Segment Down'

    @classmethod
    def poll(cls, context):
        text = context.active_object.data
        text_segment = text.text_segments[text.active_text_segment]
        text_effects = text_segment.text_effects
        return text_effects and \
            text_segment.active_text_effect < len(text_effects) - 1

    def execute(self, context):
        # Move down the effect
        text = context.active_object.data
        active_text_segment = text.active_text_segment
        text_segment = text.text_segments[active_text_segment]
        active_text_effect = text_segment.active_text_effect
        text_segment.text_effects.move(
            active_text_effect, active_text_effect + 1)
        # Point to the new index
        text_segment.active_text_effect += 1
        # Fixup FCurve paths
        swap_animation_data(
            text.animation_data, FCURVE_PATH.format(active_text_segment,'{}'),
            active_text_effect, 1)
        return {'FINISHED'}


class MoveUpTextEffect(bpy.types.Operator):
    '''Move the active text effect'''
    bl_idname = 'curve.move_up_text_effect'
    bl_label = 'Move Text Segment Up'

    @classmethod
    def poll(cls, context):
        text = context.active_object.data
        text_segment = text.text_segments[text.active_text_segment]
        return text_segment.text_effects and \
            text_segment.active_text_effect > 0

    def execute(self, context):
        # Move up the effect
        text = context.active_object.data
        active_text_segment = text.active_text_segment
        text_segment = text.text_segments[text.active_text_segment]
        active_text_effect = text_segment.active_text_effect
        text_segment.text_effects.move(
            active_text_effect, active_text_effect - 1)
        # Point to the new index
        text_segment.active_text_effect -= 1
        # Fixup FCurve paths
        swap_animation_data(
            text.animation_data, FCURVE_PATH.format(active_text_segment,'{}'),
            active_text_effect, -1)
        return {'FINISHED'}

# #############################  User Interface ##############################


class TextSegmentUIList(bpy.types.UIList):

    def draw_item(
        self, context, layout, data, item, icon,
        active_data, active_propname, index, flt_flag):

        text_curve = data
        row = layout.row(align=True)
        row.label(text=item.name if item.name else "segment", icon="DOT")
        row.prop(item, "use_enabled", text="")

    def draw_filter(self, context, layout):
        pass


class TextEffectsUIList(bpy.types.UIList):

    def draw_item(
            self, context, layout, data, item, icon,
            active_data, active_propname, index, flt_flag):

        text_segment = data
        row = layout.row(align=True)
        row.label(text=item.name if item.name else "effect", icon="DOT")
        row.prop(item, "use_enabled", text="")

    def draw_filter(self, context, layout):
        pass
        

class TEXT_PT_TextFX(bpy.types.Panel):
    '''Text Effects Panel'''
    bl_label = "Text Effects"
    bl_idname = "TEXT_PT_TextFX"
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = 'data'

    @classmethod
    def poll(cls, context):
        return context.active_object and context.active_object.type == 'FONT'

    def draw_header(self, context):
        text = context.active_object.data
        layout = self.layout
        layout.prop(text, 'use_text_FX', text="")

    def draw(self, context):
        st = context.space_data
        text = context.active_object.data
        if text.use_text_FX:
            layout = self.layout
            layout.label("Text Segments:")
            row = layout.row()
            row.template_list(
                "TextSegmentUIList", "", text, 
                "text_segments", text, "active_text_segment", type ="DEFAULT")
            col = row.column(align=True)
            col.operator("curve.add_text_segment", icon='ZOOMIN', text="")
            col.operator("curve.delete_text_segment", icon='ZOOMOUT', text="")
            col.operator(
                "curve.move_up_text_segment",
                icon='TRIA_UP', text="")
            col.operator(
                "curve.move_down_text_segment",
                icon='TRIA_DOWN', text="")
            if text.text_segments and text.active_text_segment in range(
                0, len(text.text_segments)):

                segment = text.text_segments[text.active_text_segment]
                box = layout.box()
                row = box.row()
                row.label("Active Text Segment")
                row.prop(segment, "name", text="")
                row = box.row(align=True)
                row.prop(segment, "use_datablock", text="From Data:")
                if segment.use_datablock:
                    row.prop(segment, "source_data", text="")
                    row = box.row()
                    row.label('{} Characters'.format(len(
                        bpy.data.texts[segment.source_data].as_string())))
                else:
                    row.prop(segment, "source_text", text="")
                    row = box.row()
                    row.label('{} Characters'.format(len(segment.source_text)))

                row = box.row()
                row.label("Effects List:")
                row = box.row()
                row.template_list(
                    "TextEffectsUIList", "",
                    text.text_segments[text.active_text_segment], 
                    "text_effects",
                    text.text_segments[text.active_text_segment],
                    "active_text_effect", type ="DEFAULT")

                col = row.column(align=True)
                col.operator("curve.add_text_effect", icon='ZOOMIN', text="")
                col.operator(
                    "curve.delete_text_effect", icon='ZOOMOUT', text="")
                col.operator(
                    "curve.move_up_text_effect", icon='TRIA_UP', text="")
                col.operator(
                    "curve.move_down_text_effect", icon='TRIA_DOWN', text="")

                if segment.text_effects:                
                    effect = segment.text_effects[segment.active_text_effect]
                    second = box.row()
                    col = second.column()
                    row = col.row(align=True)
                    row.prop(effect, "name", text="Current Effect:")
                    row = col.row(align=True)
                    row.prop(effect, "progression", text="")
                    row.prop(effect, "target", text="To")     
                    row = col.row(align=True)
                    if effect.use_count:
                        row.prop(effect, "count", text="Count")
                    else:
                        row.prop(effect, "anim", text="Time")
                    row.prop(effect, "use_count", text="Use Count")
                    row = col.row(align=True)
                    if effect.progression == 'RANDOM':
                        row.prop(effect, "seed", text="Seed")
                    else:
                        row.label("   ")
                    if effect.target == 'MATERIAL':
                        row.prop(effect, "material_index", text="Material ID")
                    elif effect.target in [
                        'BOLD', 'UNDERLINE', 'ITALIC', 'SMALLCAPS']:

                        row.prop(effect, "invert", text="Flip")
                    else:
                        row.label("   ")

# ############################# Registration #################################


def register():
    '''
    addon registration function
    '''
    # create properties
    add_properties()
    # register UI Lists:
    bpy.utils.register_class(TextSegmentUIList)
    bpy.utils.register_class(TextEffectsUIList)
    # register operators:
    bpy.utils.register_class(AddTextEffect)
    bpy.utils.register_class(AddTextSegment)
    bpy.utils.register_class(DelTextEffect)
    bpy.utils.register_class(DelTextSegment)
    bpy.utils.register_class(MoveUpTextEffect)
    bpy.utils.register_class(MoveUpTextSegment)
    bpy.utils.register_class(MoveDownTextEffect)
    bpy.utils.register_class(MoveDownTextSegment)
    bpy.utils.register_class(CopyTextToSegment)
    # register UI:
    bpy.utils.register_class(TEXT_PT_TextFX)
    # add the frame change handler
    bpy.app.handlers.frame_change_post.append(text_fx_update_frame)


def unregister():
    '''
    addon unregistration function
    '''
    # remove the frame change handler
    bpy.app.handlers.frame_change_post.remove(text_fx_update_frame)
    # remove the panel
    bpy.utils.unregister_class(TEXT_PT_TextFX)
    # remove the operators:
    bpy.utils.unregister_class(AddTextEffect)
    bpy.utils.unregister_class(AddTextSegment)
    bpy.utils.unregister_class(DelTextEffect)
    bpy.utils.unregister_class(DelTextSegment)
    bpy.utils.unregister_class(MoveUpTextEffect)
    bpy.utils.unregister_class(MoveUpTextSegment)
    bpy.utils.unregister_class(MoveDownTextEffect)
    bpy.utils.unregister_class(MoveDownTextSegment)
    bpy.utils.unregister_class(CopyTextToSegment)
    # remove the UI Lists:
    bpy.utils.unregister_class(TextSegmentUIList)
    bpy.utils.unregister_class(TextEffectsUIList)
    # remove the properties
    del_properties()

if __name__ == "__main__":
    register()
